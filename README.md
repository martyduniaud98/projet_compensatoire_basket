# Projet_Compensatoire_Basket


## Table of contents

- [Trello](#trello)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Run the project](#run-the-project)
- [Screenshots](#screenshots)
- [Author](#author)


## Trello

That's the link of my trello :
https://trello.com/b/DCH9KpxQ/projetcompensatoire

<br>

## Prerequisites

- [Python3](https://www.python.org/downloads/) (version 3.10.6)
- Virutalenv : `pip install virtualenv`

<br>

## Installation

- Clone the repository : `git clone https://gitlab.com/martyduniaud98/projet_compensatoire_basket`


- ### Install & Activate virtual environnement 
  <br>

  In Linux terminal :
  ```
  projet_compensatoire_basket>$ python3 -m venv myenv
  projet_compensatoire_basket>$ source myenv/bin/activate
  ```

  In Windows terminal :
  ```
  projet_compensatoire_basket>$ python -m venv myenv
  projet_compensatoire_basket>$ myenv\Scripts\activate
  ```
  <br>

- ### Install dependencies

  ```
  (myenv) projet_compensatoire_basket>$ pip install Flask requests hashlib functools
  ```

  <br>

## Run the project

```
(myenv) projet_compensatoire_basket>$ flask run
```

<br>

## Screenshots

- Home page
![](./static/img/screen_home.png)

- Login page
![](./static/img/screen_login.png)


## Author

- [Duniaud Marty](https://gitlab.com/martyduniaud98)