from flask import Flask, render_template, redirect, url_for, request, session, flash
import requests
import datetime
import sqlite3
from functools import wraps
import hashlib

app = Flask(__name__)
app.secret_key = 'myreallyreallyreallyreallysecretkey'


def db_connection():
    conn = None
    try:
        conn = sqlite3.connect('users.sqlite')
    except sqlite3.error as e:
        print(e)
    return conn


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' not in session:
            flash('You need to login for access to this page.')
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated_function


@app.route('/register', methods=['GET', 'POST'])
def register():
    conn = db_connection()
    cursor = conn.cursor()

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        conf_pass = request.form['conf_password']

        # verification if user already exists
        cursor.execute(
            "SELECT COUNT(*) FROM users WHERE username = ?", (username,))
        count = cursor.fetchone()[0]
        if password != conf_pass:
            error = "Passwords don't match. Please try again."
            conn.close()
            return render_template('register.html', error=error)

        if count > 0:
            error = "Username or password already exists. Please try again."
            conn.close()
            return render_template('register.html', error=error)

        # hash password
        salt = "S4lt"
        salty_password = password+salt
        hashed_password = hashlib.sha512(salty_password.encode()).hexdigest()

        # add user to database
        cursor.execute(
            "INSERT INTO users (username, password) VALUES (?, ?)", (username, hashed_password))
        conn.commit()
        conn.close()
        return redirect(url_for('login'))

    return render_template('register.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    username = None

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        # hash password
        salt = "S4lt"
        salty_password = password + salt
        hashed_password = hashlib.sha512(salty_password.encode()).hexdigest()

        # Verification if user exists
        user = db_connection().execute(
            "SELECT * FROM users WHERE username = ? AND password = ?", (
                username, hashed_password)
        ).fetchone()

        if user:
            session['username'] = username
            return redirect(url_for('players'))
        else:
            error = 'Invalid username or password. Please try again.'

    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    # DELETE username from session
    session.pop('username', None)
    return redirect(url_for('login'))


def date_format(date):
    simple_date = date[0:10]
    date_datetime = datetime.datetime.fromisoformat(simple_date)
    date_formated = date_datetime.strftime("%d-%m-%Y")
    return date_formated


@app.route('/')
@login_required
def players():
    response = requests.get('https://www.balldontlie.io/api/v1/players')
    players = response.json()['data']
    return render_template('players.html', players=players)


@app.route('/player/<int:id>')
@login_required
def player(id):
    url = f'https://www.balldontlie.io/api/v1/players/{id}'
    response = requests.get(url)
    resp_json = response.json()
    player = {
        'id': resp_json['id'],
        'first_name': resp_json['first_name'],
        'last_name': resp_json['last_name'],
        'position': resp_json['position'],
        'feet': resp_json['height_feet'],
        'inches': resp_json['height_inches'],
        'pounds': resp_json['weight_pounds'],
        'team': {
            'id': resp_json['team']['id'],
            'abbrev': resp_json['team']['abbreviation'],
            'city': resp_json['team']['city'],
            'conf': resp_json['team']['conference'],
            'div': resp_json['team']['division'],
            'full_name': resp_json['team']['full_name'],
            'name': resp_json['team']['name']
        }
    }
    return render_template('details_player.html', player=player)


@app.route('/teams')
@login_required
def teams():
    response = requests.get('https://www.balldontlie.io/api/v1/teams')
    teams = response.json()['data']
    return render_template('teams.html', teams=teams)


@app.route('/team/<int:id>')
@login_required
def team(id):
    url = f'https://www.balldontlie.io/api/v1/teams/{id}'
    response = requests.get(url)
    resp_json = response.json()
    team = {
        'id': resp_json['id'],
        'abbrev': resp_json['abbreviation'],
        'city': resp_json['city'],
        'conf': resp_json['conference'],
        'div': resp_json['division'],
        'full_name': resp_json['full_name'],
        'name': resp_json['name']
    }
    response = requests.get(
        f'https://www.balldontlie.io/api/v1/players?team_ids[]={id}')
    players = response.json()['data']
    return render_template('details_team.html', team=team, players=players)


@app.route('/matches')
@login_required
def matches():
    response = requests.get('https://www.balldontlie.io/api/v1/games')
    matches = response.json()['data']

    for match in matches:
        date_json = match['date']
        date_formated = date_format(date_json)
        match['date_formated'] = date_formated

    return render_template('matches.html', matches=matches)


@app.route('/match/<int:id>')
@login_required
def match(id):
    url = f'https://www.balldontlie.io/api/v1/games/{id}'
    response = requests.get(url)
    resp_json = response.json()
    match = {
        'id': resp_json['id'],
        'date': resp_json['date'],
        'home_team_score': resp_json['home_team_score'],
        'visitor_team_score': resp_json['visitor_team_score'],
        'season': resp_json['season'],
        'period': resp_json['period'],
        'status': resp_json['status'],
        'time': resp_json['time'],
        'home_team': {
            'id': resp_json['home_team']['id'],
            'abbrev': resp_json['home_team']['abbreviation'],
            'city': resp_json['home_team']['city'],
            'conf': resp_json['home_team']['conference'],
            'div': resp_json['home_team']['division'],
            'full_name': resp_json['home_team']['full_name'],
            'name': resp_json['home_team']['name']
        },
        'visitor_team': {
            'id': resp_json['visitor_team']['id'],
            'abbrev': resp_json['visitor_team']['abbreviation'],
            'city': resp_json['visitor_team']['city'],
            'conf': resp_json['visitor_team']['conference'],
            'div': resp_json['visitor_team']['division'],
            'full_name': resp_json['visitor_team']['full_name'],
            'name': resp_json['visitor_team']['name']
        }
    }
    response = requests.get('https://www.balldontlie.io/api/v1/teams')
    teams = response.json()['data']
    return render_template('details_match.html', match=match, date_formated=date_format(match['date']), teams=teams)


if __name__ == '__main__':
    app.run(debug=True)
