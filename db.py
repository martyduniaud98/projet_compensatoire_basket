import sqlite3

conn = sqlite3.connect('users.sqlite')

cursor = conn.cursor()
sql_query = """ CREATE TABLE IF NOT EXISTS users (
    id integer PRIMARY KEY,
    username text NOT NULL,
    password text NOT NULL
)"""

cursor.execute(sql_query)